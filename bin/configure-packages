#!/usr/bin/env bash
#
# Usage: ./bin/configure-packages <packages>
# If no arguments, will do '--all'
#

package="${1:---all}"
source .gitlab-token

if [ ! -e ./salsa/auth.conf ] || [ ! -e ./salsa/packages.conf ]; then
  echo "ERROR: You don't seem to be in the correct directory" >&2
  echo "Run this tool from the top-level directory as: ./bin/configure-packages" >&2
  exit 1
fi

if [ -z "${SALSA_TOKEN}" ]; then
  echo "ERROR: You are missing ./.gitlab-token" >&2
  echo "Create: https://gitlab.com/-/profile/personal_access_tokens" >&2
  echo "Afterwards: echo \"SALSA_TOKEN='XXX'\" > ./.gitlab-token" >&2
  exit 1
fi

if [ "${package}" = "--all" ]; then
  # This is done, as --all would be doing BOTH --archived and --no-archived
  # 5034987 = https://gitlab.com/kalilinux/packages
  echo "[i] No selected Package. Doing --all. Fetching packages list..."
  package=$(salsa --conf-file ./salsa/auth.conf \
        --group-id 5034987 \
        --no-archived \
        ls \
    | awk '/Name:/ { print $2 }' \
    | sort)

  echo "[i] Total: $(echo "${package}" | wc -l) packages"
fi

salsa \
  --conf-file ./salsa/auth.conf \
  --conf-file +./salsa/packages.conf \
  --debug \
  --verbose \
  --no-fail \
  update_safe ${package}
