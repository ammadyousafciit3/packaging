#!/usr/bin/env sh
#
# Refresh mrconfig with active repos
# After running, may wish to execute:
#   - $ ./bin/setup-team-repo   (Pull any new repos)
#   - $ ./bin/archive-team-repo (Move any old repos)
#   - $ ./bin/auto-update       (Configure repos)

#
# Usage: ./bin/update-mrconfig
# There are no arguments
#

set -e

export LC_ALL=C

## 5034987 = https://gitlab.com/kalilinux/packages
projects=$(salsa --conf-file ./salsa/auth.conf --group-id 5034987 ls | awk '/Name:/ {print $2}' | sort)

if [ -z "${projects}" ]; then
  echo "[-] Unable to get any repos" 1>&2
  exit 1
fi

cat <<EOF> mrconfig
[DEFAULT]
jobs = 4
# Ignore svn/cvs metadata
svn_test = false
cvs_test = false
# Checkout the debian branch if it exists
post_checkout =
	cd \$MR_REPO
	if git show-ref -q --verify refs/remotes/origin/debian; then
	    git branch --track debian origin/debian
	fi
# Update the 3 branches, plus the debian branch if it exists
update =
	gbp pull
	if git show-ref -q --verify refs/remotes/origin/debian; then
	    if ! git show-ref -q --verify refs/heads/debian; then
	        git branch --track debian origin/debian
	    fi
	    git fetch origin debian:debian
	fi
EOF

for project in ${projects}; do
  cat <<EOF>> mrconfig

[${project}]
checkout = gbp clone --defuse-gitattributes=off --debian-branch=kali/master git@gitlab.com:kalilinux/packages/${project}.git
EOF
done
